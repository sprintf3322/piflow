package cn.piflow.flow

import cn.piflow.FlowEngine
import cn.piflow.rpc.FlowEngineServer
import org.apache.spark.sql.SparkSession

/**
	* Created by bluejoe on 2017/10/28.
	*/
object shared {
	val spark = SparkSession.builder.master("local[4]")
		.getOrCreate();

	lazy val localRunner = {
		spark.conf.set("spark.sql.streaming.checkpointLocation", "/tmp");
		FlowEngine.startLocal(spark)
	};

	lazy val remoteRunner = {
		FlowEngineServer.main(Array("1224", "/rpc"));
		FlowEngine.connect("http://localhost:1224/rpc")
	};
}
